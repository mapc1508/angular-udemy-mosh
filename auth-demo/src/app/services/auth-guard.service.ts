import { Injectable } from '@angular/core';
import { CanActivate, Router, RouterStateSnapshot } from '../../../node_modules/@angular/router';
import { AuthService } from './auth.service';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private authService: AuthService, private router: Router) { }

  // RouterStateSnapshot is an optional parameter of the canActivate interface that has the information of the url user wanted to access
  canActivate(route, state: RouterStateSnapshot) {
    if (this.authService.isLoggedIn()) return true;

    // want to use additional queryStringParam (returnUrl) to navigate the user to the desired page he requested before needed to sign-in
    // We need to handle the returnUrl property in the loging component
    this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
    return false;
  }

}

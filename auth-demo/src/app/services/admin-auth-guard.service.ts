import { Injectable } from '@angular/core';
import { CanActivate, Router } from '../../../node_modules/@angular/router';
import { AuthService } from './auth.service';

@Injectable()
export class AdminAuthGuard implements CanActivate {

  constructor(private router: Router, private authService: AuthService) { }

  canActivate() {
    // want to allow user to access the page only if he is in the admin role
    // should check that user is truthy
    let user = this.authService.currentUser;
    if (user && user.admin) return true;

    this.router.navigate(['/no-acceess']);
    return false;
  }

}

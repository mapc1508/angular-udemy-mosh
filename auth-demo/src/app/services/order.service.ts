import { Http, RequestOptions, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { AuthHttp } from '../../../node_modules/angular2-jwt';

@Injectable()
export class OrderService {

  // if we need both the standard http service for the non-protected api calls and authHttp for others we can define them both in the constructor
  constructor(private http: AuthHttp) { }

  getOrders() {
    // we are not getting the response because the authorization headers are missing
    // return this.http.get('/api/orders')
    //   .map(response => response.json());

    // // need to add the header to the request and append its name and value
    // let headers = new Headers();
    // let token = localStorage.getItem('token');
    // headers.append('authorization', 'Bearer ' + token);

    // // need to create request options object
    // // compilation error, different type of the supplied argument and expected
    // // the Headers type we used to create headers object is nativelly defined in the browser, but we need one defined in Angular
    // // need to explicitly import Headers type from angular/http
    // let options = new RequestOptions({ headers: headers});

    // writing all the code required to pass the options object in all secured api calls is repetitive
    // we can use AuthHttp class angular2-jwt instead of plain Http class from angular library
    // the main differnce is that AuthHttp contains all the methods from Http class but also includes the Auth logic we used to set the secure header for the api

    // need to pass the optional options argument when calling the api
    // return this.http.get('/api/orders', options)
    //   .map(response => response.json());

    return this.http.get('/api/orders')
      .map(response => response.json());

    // when securing the api on the server, we should always check for the existance of the authorization header
  }
}

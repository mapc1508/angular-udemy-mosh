export class Point { //export keyword is used to make the class visible outside the current file
//exporting something makes the file a module in TS
    constructor(private _x?: number, private _y?: number) { } 

    draw() {
        console.log('X: ' + this._x + ', Y: ' + this._y); 
    }
}

//Keeping classes and definitions in multiple files is always better. Should be kept in a separate module


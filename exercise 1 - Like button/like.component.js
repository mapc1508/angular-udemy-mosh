"use strict";
exports.__esModule = true;
var LikeComponent = /** @class */ (function () {
    function LikeComponent(_likesCount, _isSelected) {
        this._likesCount = _likesCount;
        this._isSelected = _isSelected;
    }
    Object.defineProperty(LikeComponent.prototype, "likesCount", {
        get: function () {
            return this._likesCount;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LikeComponent.prototype, "isSelected", {
        get: function () {
            return this._isSelected;
        },
        enumerable: true,
        configurable: true
    });
    LikeComponent.prototype.onClick = function () {
        // if (this.isSelected) {
        //     this.likesCount++;
        // } else{
        //     this.likesCount--;
        // }
        this._isSelected = !this._isSelected;
        this._likesCount += (this._isSelected) ? 1 : -1;
    };
    return LikeComponent;
}());
exports.LikeComponent = LikeComponent;

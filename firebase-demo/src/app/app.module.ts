import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
// importing firebase module for angular
import { AngularFireDatabaseModule } from '../../node_modules/angularfire2/database';
import { AngularFireModule } from '../../node_modules/angularfire2';

import { environment } from '../environments/environment';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    // need to pass the configuration object for the firebase DB (import environment object)
    AngularFireModule.initializeApp(environment.firebase),
    // another firebase module that we need since we are using firebase database
    AngularFireDatabaseModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

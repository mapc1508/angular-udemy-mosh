import { Component } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  private courses: AngularFireList<any>;
  courses$;

  constructor(private db: AngularFireDatabase) {
    this.courses = db.list('/courses');
    this.courses$ = this.courses.snapshotChanges();
  }

  add(course: HTMLInputElement) {
    this.courses.push({
      name: course.value,
      price: 150,
      isLive: true,
      sections: [
        { title: 'Components' },
        { title: 'Directives' },
        { title: 'Templates' }
      ]
    });
    course.value = '';
  }

  update(course) {
    this.db.object('/courses/' + course.key)
      .set({
        title : course.payload.val() + ' UPDATED',
        price: 150
      });
  }

  delete(course) {
    // removing db object based on its key
    this.db.object('/courses/' + course.key)
      .remove()
      // all the crud methods return a promise
      .then(x => console.log('DELETED'));
      // .catch();
  }
}

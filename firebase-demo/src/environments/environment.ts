// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

// in the real-case scenario we should have separte firebase configuration for the production environment.prod.ts
export const environment = {
  production: false,
  // need to import configuration from our firebase project
  firebase: {
    apiKey: 'AIzaSyA3vp4pY4lBZWz_mz4TrrAWZax2gtleM9s',
    authDomain: 'fir-demo-97521.firebaseapp.com',
    databaseURL: 'https://fir-demo-97521.firebaseio.com',
    projectId: 'fir-demo-97521',
    storageBucket: 'fir-demo-97521.appspot.com',
    messagingSenderId: '54540170151'
  }
};

// defining an interface that will be used as model for data retrieved from the user db object
export interface AppUser {
    name: string;
    email: string;
    isAdmin: boolean;
}

import { Injectable } from '@angular/core';
import { CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { AuthService } from './auth.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private auth: AuthService, private router: Router) { }

  // takes 2 params: route and state
  // can get the route that user tried to access when the authGuard checked it
  canActivate(route, state: RouterStateSnapshot) {

    return this.auth.user$.pipe(map(user => {
      if (user) {
        return true;
      }

      // can use navigation extras object. Have the queryParams property for sending optional parameters
      // since we use the google authentication the additional parameter will be lost once the google authentication is finished
      // need to store it in the local storage
      this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
      return false;
    }));
  }
}

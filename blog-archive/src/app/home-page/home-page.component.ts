import { Component, OnInit } from '@angular/core';
import { Archive } from '../archive';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {

  archives: Archive[];

  constructor() {
    this.archives = [
      {
        year: 2018,
        month: 1
      },
      {
        year: 2018,
        month: 2
      },
      {
        year: 2018,
        month: 3
      }
  ];
   }

  ngOnInit() {
  }

}

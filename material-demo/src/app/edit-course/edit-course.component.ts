import { Component, OnInit, InjectionToken, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '../../../node_modules/@angular/material/dialog';

// export const DIALOG_DATA = new InjectionToken('DIALOG_DATA');

@Component({
  selector: 'app-edit-course',
  templateUrl: './edit-course.component.html',
  styleUrls: ['./edit-course.component.css']
})
export class EditCourseComponent implements OnInit {

  // error returned: Can't resolve all parameters for EditCourseComponent
  // Angular does not know what to inject in the constructor (parameter any)
  // constructor(private data: any) { console.log('Data: ' + data); }
  // need to use Inject() decorator for the injection
  // constructor(@Inject(DIALOG_DATA) data: any) { console.log('Data: ' + data); }

  // MAT_DIALOG_DATA - injection token for the object passed to dialog
  constructor(@Inject(MAT_DIALOG_DATA) data: any) { console.log('Data:', data); }

  ngOnInit() {
  }

}

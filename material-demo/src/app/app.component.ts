import { Component } from '@angular/core';
import { timer } from 'rxjs';
import { MatDialog } from '../../node_modules/@angular/material/dialog';
import { EditCourseComponent } from './edit-course/edit-course.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private dialog: MatDialog) {

  }

  openDialog() {
    // if want to pass the data to a dialog, we need pass an object as 2. argument of the open()
    this.dialog.open(EditCourseComponent, {
      data: { courseId: 1 }
    })
      .afterClosed()
      .subscribe(result => console.log(result));
  }
}


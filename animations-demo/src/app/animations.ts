import { trigger, state, transition, style, animate, keyframes, animation, useAnimation } from '../../node_modules/@angular/animations';

export let bounceOutLeftAnimation = animation(
    animate('1s ease-out', keyframes([
        style({
            offset: 0.2,
            opacity: 1,
            transform: 'translateX(20px)'
        }),
        style({
            offset: 1,
            opacity: 0,
            transform: 'translateX(-100%)'
        })
    ]))
);

// making 2 reusable animations
// issue here is the hardcoded duration, not flexible if want different duration in different host components
// can add parameters to reusable animations
export let fadeInAnimation = animation([
    style({ opacity: 0 }),
    // want to make the values dynamic
    // animate('2s ease-out')
    // should use interpolation
    animate('{{ duration }} {{ easing }}')
    
    // as a second argument to animation function we can use optional AnimationOptions object
    // AnimationOptions takes 2 optional params: 1. delay, 2. params list (key-value pairs) can be used to supply default values for the parameters
], {
    params: {
        // default values, consumer of the animation function can override them
        duration: '2s',
        easing: 'ease-out'
    }
})

export let fadeOutAnimation = animation([
    animate(2000, style({ opacity: 0 }))
])

export let fade =
    trigger('fade', [
        // state('void', style({ opacity: 0 })),
        // the code here is not reusable in other transitions
        // we have defined the state object that is outside the transition function
        // we can only reuse what's inside the transition function using animation()
        // need to separate the transition states and delete state function
        // transition(':enter, :leave', [
        //     animate(2000)
        // ])
        transition(':enter', [
            useAnimation(fadeInAnimation)
        ]),
        transition(':enter, :leave', [
            useAnimation(fadeOutAnimation)
        ])
    ]);

export let slide =
    trigger('slide', [
        transition(':enter', [
            style({ transform: 'translateX(-20px)' }),
            animate(1000)
        ]),
        transition(':leave',
            useAnimation(bounceOutLeftAnimation)
        )
    ])

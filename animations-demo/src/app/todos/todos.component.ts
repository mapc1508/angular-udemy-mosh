import { Component } from '@angular/core';
import { trigger, state, transition, style, animate, keyframes, useAnimation, query, animateChild, group, stagger } from '../../../node_modules/@angular/animations';
import { bounceOutLeftAnimation, fadeInAnimation } from '../animations';

@Component({
  selector: 'todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css'],
  animations: [
    trigger('todosAnimation', [
      transition(':enter', [
        group([
          query('h1', [
            style({ transform: 'translateY(-20px)' }),
            animate(1000)
          ]),
          // getting todo items by class selector
          // if we reference the child element by class selector and don't apply animation on each of child elements the animation is going to be called only once (when the parent is initialized)
          // query('.list-group-item', [

          query('@todoAnimation', [
            // animateChild()
            // want each todo item's animation to be run one after another
            // we can use stagger helper function to achieve this (curtain-like effect)
            // first argument is timing, and second is animation 
            stagger(200, animateChild() // stagger can only be used inside the query function

              // (can include multiple steps for the animation, reuse existing using useAnimation()...)
              // style({ opacity: 0, transform: 'translateX(-20px)' }),
              // animate(1000)
            )
          ])
        ])
      ])
    ]),
    trigger('todoAnimation', [
      transition(':enter', [
        useAnimation(fadeInAnimation, {
          params: {
            duration: '500ms'
          }
        })
      ]),
      transition(':leave', [

        style({ backgroundColor: 'red' }),
        animate(1000),
        useAnimation(bounceOutLeftAnimation)
      ])
    ])
  ]
})
export class TodosComponent {
  items: any[] = [
    'Wash the dishes',
    'Call the accountant',
    'Apply for a car insurance'];

  addItem(input: HTMLInputElement) {
    this.items.splice(0, 0, input.value);
    input.value = '';
  }

  removeItem(item) {
    let index = this.items.indexOf(item);
    this.items.splice(index, 1);
  }

  animationStarted($event) {
    console.log($event);
  }

  animationDone($event) {
    console.log($event);
  }
}

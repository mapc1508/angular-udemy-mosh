import { Component, Input } from '@angular/core';
import { expandCollapse } from './zippy.component.animations';

// the issue we have is the separtion of concerns principle
// animations logic is extra noice, the focus in this file should be on the component
// the same reason why styles and templates are kept in a separate file

@Component({
  selector: 'zippy',
  templateUrl: './zippy.component.html',
  styleUrls: ['./zippy.component.css'],
  animations: [
   expandCollapse
  ]
})
export class ZippyComponent {
  @Input('title') title: string;
  isExpanded: boolean;

  toggle() {
    this.isExpanded = !this.isExpanded;
  }
}

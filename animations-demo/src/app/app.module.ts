import { ZippyComponent } from './zippy/zippy.component';
import { TodosComponent } from './todos/todos.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
// The module needed to use angular animations in browsers
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent,
    TodosComponent,
    ZippyComponent
  ],
  imports: [
    BrowserModule,
    // implemented on top of Web Animations API
    // need to import polyfills so that it can be used with all browsers like IE or Edge
    // polyfill - code that allows us to use modern JS features in older browsers
    // we have polyfills.ts file in angular that we use to enable them
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

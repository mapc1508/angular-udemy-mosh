import { AbstractControl, ValidationErrors } from "@angular/forms";

export class UsernameValidators {
    static cannotContainSpace(control: AbstractControl): ValidationErrors | null {
        if ((control.value as string).indexOf(' ') >= 0) {
            return {
                cannotContainSpace: true
            };
        }
        return null;
    }

    // Promise is returned beacuse we use async operation
    static shouldBeUnique(control: AbstractControl): Promise<ValidationErrors | null> {
        // Promise constructor takes a function as an argument (with two parameters, resolve and reject) and returns void
            // resolve is also a function that returns the value to the caller of the promise
            // reject function returns error messages to the caller
        return new Promise ((resolve, reject) => {
            setTimeout(() => {
                if (control.value === 'mosh') {
                    // takes parameter of type Any and returns void
                    resolve ({ shouldBeUnique: true });
                } else {
                    reject (null);
                };
            }, 2000);
        })
    }
}

import { Component, OnInit, Input, Output, EventEmitter, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'favorite',
  templateUrl: './favorite.component.html',
  styleUrls: ['./favorite.component.css'],
  // view encapsulation
    // ViewEncapsulation enums:
      // 1. Emulated - emulates concept of shadow DOM, uses internal 'trick' to scoped the style to the component DEFAULT
      // 2. Native - native shadow DOM, not supported by all browsers
      // 3. None - styles will leak outsed the template, should not be used
  // encapsulation: ViewEncapsulation.None
})
export class FavoriteComponent {
  @Input('isFavorite') isFavorite: boolean;
  @Output('change') click = new EventEmitter();

  onClick() {
    this.isFavorite = !this.isFavorite;

    this.click.emit({ newValue: this.isFavorite });
  }

}

export interface FavoriteChangedEventArgs {
  newValue: boolean
}


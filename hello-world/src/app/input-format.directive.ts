import { Directive, HostListener, ElementRef, Input } from '@angular/core';
// HostListener - decorator function that allows subscribing to DOM elements that are hosting the directive (has the directive attribute)
// ElementRef - service that gives the reference to a DOM object

// directive decorator function
// want to handle 2 DOM events: focus, and blur
@Directive({
  selector: '[appInputFormat]' // if angular finds element with this attribute it will apply the custom directive to that element
})
export class InputFormatDirective {
  // called when input field has focus
  // @HostListener('focus') onFocus() {
  //   console.log('On focus.')
  // }

  // if want to inject the value of this field directly in the directive should set the alias to the name of the directive's selector
  @Input('appInputFormat') format;

  constructor(private el: ElementRef) {
    // need a reference of the host element to use it with HostListener function
  }

  @HostListener('blur') onBlur() {
    // nativeElement - actual DOM object
    let value: string = this.el.nativeElement.value;
    console.log(this.format);

    if (this.format === 'lowercase') {
      this.el.nativeElement.value = value.toLowerCase();
    } else {
      this.el.nativeElement.value = value.toUpperCase();
    }
  }

}

import { Component, OnInit } from '@angular/core';
import { GithubFollowersService } from '../services/github-followers.service';
import { ActivatedRoute } from '../../../node_modules/@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/combineLatest'
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/switchMap'


@Component({
  selector: 'github-followers',
  templateUrl: './github-followers.component.html',
  styleUrls: ['./github-followers.component.css']
})
export class GithubFollowersComponent implements OnInit {

  followers: any[];

  constructor(public service: GithubFollowersService, private route: ActivatedRoute) { }

  ngOnInit() {
    Observable.combineLatest([this.route.paramMap, this.route.queryParamMap])
    // every item of the combineLatest observable is of type ParamMap[]
    // can map ParamMap[] into Followers[]
      // .map(combined => {
      .switchMap(combined => {
        // what is defined as output of the map function becomes the input to the subscribe()
        let id = combined[0].get('id');
        let page = combined[1].get('page');

        // getAll() returns Observable<any>
        // we want followers[] in the subscribe method, not Observable<followers[]>
        // to fix this we need to use switchMap operator which returns the actual items of the Observable
        return this.service.getAll();
      })
      .subscribe(followers => { this.followers = followers });
        // let id = combined[0].get('id');
        // let page = combined[1].get('page');

        // this.service.getAll()
          // having subscribe method inside another one
          // need cleaner solution
          // .subscribe(followers => {
          //   this.followers = followers;
          // });
      // });
  }

}

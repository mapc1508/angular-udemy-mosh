// need to import Pipe decorator and PipeTransform interface that defines the structure of any pipe
// need to register it in the app module
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'summary' // defines the name of the pipe used in the html
})
export class SummaryPipe implements PipeTransform {
    // need to implement the transform method that is inhereted from the parent class
    transform(value: string, limit?: number) {
        if (!value) {
            return null;
        }

        let actualLimt = (limit) ? limit : 50; // if an argument is supplied to a pipe it is used, otherwise default is 50

        return value.substr(0, limit) + '...'; // when the string is provided as argument to a pipe, it returns firts 50 chars of that string
    }
}

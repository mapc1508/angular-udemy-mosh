import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/catch'
import { Observable } from 'rxjs/observable';
import { AppError } from '../common/app.error';
import { NotFoundError } from '../common/not-found-error';
import { BadRequestError } from '../common/bad-request-error';
import 'rxjs/add/observable/throw'
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/toPromise'

@Injectable({
    providedIn: 'root'
})
export class DataService {

    constructor(private url: string, private http: Http) { }

    getAll() {
        return this.http.get(this.url)
            .map(response => response.json())
            .catch(this.handleError);
    }

    create(resource) {
        return this.http.post(this.url, JSON.stringify(resource))
            .map(response => response.json())
            .catch(this.handleError);
    }

    update(resource) {
        return this.http.put(this.url + '/' + resource.id, JSON.stringify(resource))
            .map(response => response.json())
            .catch(this.handleError);
    }

    delete(id) {
        // simulating error request
        // return Observable.throw(new AppError());

        return this.http.delete(this.url + '/' + id)
            .map(response => response.json())
            // another powerful observable operator that calls the service three times in cases of failure
            // .retry(3)
            // this method is used to convert observable to promise. This is not the best practice, should stick with Observables when dealing with async http calls
            .toPromise()
            .catch(this.handleError);

            // all the operators come to effect only when the user of the service is subscribed
    }

    private handleError(error: Response) {
        if (error.status === 400)
            return Observable.throw(new BadRequestError(error.json()));
        else if (error.status === 404)
            return Observable.throw(new NotFoundError());

        return Observable.throw(new AppError(error.json()));
    }
}

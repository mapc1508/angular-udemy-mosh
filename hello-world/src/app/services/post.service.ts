// clean the import statements
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { DataService } from './data.service';

@Injectable()
export class PostService extends DataService {

  // issue with inheritance: both classes contain same private url field
  // since it is defined in the base class, it should not be re-defined in this class
  // private url = 'http://jsonplaceholder.typicode.com/posts';

  // another error: both classes have separate declarations of the private property - http
  // constructor(private http: Http) { }

  // new error: Constructor for the derived class must contain a 'super' call
  // when creating derived object, base object needs to be created first
  // to create it, we need to provide it with the http property in its constructor
  constructor(http: Http) {
    // passing the parameter to the base class' constructor
    super('http://jsonplaceholder.typicode.com/posts', http);
  }

  // now post service has only few lines of code

  // deleted all the specific methods, want to use global dataService
}

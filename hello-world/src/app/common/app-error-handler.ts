import { ErrorHandler } from '@angular/core';

// responsable for handling all unexpected errors within the app
// Angular contains predefined ErrorHandler class
// Has handlerError(error: any): void
// By default only logs the error to the console
// Need differnt implementation of the class - need to also log the error to the server
// need to define this in the app module as a provider to have it available in all components
export class AppErrorHandler implements ErrorHandler {
    handleError(error: any): void {
        alert('An unexpected error occured');
        console.log(error);
    }
}

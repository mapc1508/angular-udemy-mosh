import { AppError } from './app.error';

// inheriting from the general AppError base class
export class NotFoundError extends AppError {
    
}

import { Injectable } from '@angular/core'; //another decorator function. Needed only if the service has dependencies in its constructor

@Injectable()
export class EmailService {

  // constructor(logService: LogService) { }  - making a class injectable enables angular to inject dependencies of the class in the constructor. 
  //Component decorator automatically includes injectable 

  constructor() {
    
  }

}

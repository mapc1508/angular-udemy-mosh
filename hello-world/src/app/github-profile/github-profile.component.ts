import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '../../../node_modules/@angular/router';

@Component({
  selector: 'app-github-profile',
  templateUrl: './github-profile.component.html',
  styleUrls: ['./github-profile.component.css']
})
export class GithubProfileComponent implements OnInit {
  // need router service to programatically send user to a different route
  constructor(private router: Router) {

  }

  ngOnInit() {}

  submit() {
    // navigate function takes the same array of parameters like routerLink as the first param
    // second argument is navigation extra object
    // in it we have queryParams object where can set additional query string properties
    this.router.navigate(['/followers'], {
      queryParams: {page: 1, order: 'newest'}
    });
  }

}

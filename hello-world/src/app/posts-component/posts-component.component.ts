import { Component, OnInit } from '@angular/core';
import { PropertyBindingType } from '@angular/compiler';
import { PostService } from '../services/post.service';
import { AppError } from '../common/app.error';
import { NotFoundError } from '../common/not-found-error';
import { BadRequestError } from '../common/bad-request-error';

@Component({
  selector: 'posts-component',
  templateUrl: './posts-component.component.html',
  styleUrls: ['./posts-component.component.css']
})
export class PostsComponentComponent implements OnInit {

  posts: any[];

  constructor(public service: PostService) {
  }

  ngOnInit(): void {
    this.service.getAll()
      .subscribe(posts => this.posts = posts);
  }

  createPost(input: HTMLInputElement) {
    let post = { title: input.value }
    // optimistic updates: assuming positive outcome most of the time and showing new object before it is validated by the API - subscribe()
    // faster and more user-friendly
    this.posts.splice(0, 0, post);

    input.value = '';

    this.service.create(post)
      .subscribe(
        newPost => {
          post['id'] = newPost.id;

          // pessimistic approach: only changing presentation once it is backed up by the call to API
          // this.posts.splice(0, 0, post);

          console.log(newPost);
        },
        (error: AppError) => {
          // optimistic: only if error occurs we rollback the change and delete the new item
          this.posts.splice(0, 1);

          if (error instanceof BadRequestError) {
          }
          else {
            throw error;
          }
        });
  }

  updatePost(post) {
    this.service.update(post)
      .subscribe(updatedPost => {
        console.log(updatedPost);
      });
  }

  deletePost(post) {
    // let index = this.posts.indexOf(post);
    // this.posts.splice(index, 1);

    // this.service.delete(post.id)
    //   .subscribe(
    //     () => { },
    //     (error: AppError) => {
    //       this.posts.splice(0, 0, post);

    //       if (error instanceof NotFoundError)
    //         alert('This post has already been deleted.');

    //       else {
    //         throw error;
    //       }
    //     });

    // if we don't subscribe to a delete method that returns Observable the service will not be called
    // with Observables, nothing happens until you subscribe to them
    // service will not call the backend if it is simply called, only when .subscribe()
    // Observables are lazy, while promises are eager
    // they are both used when inititating asyncrhonous operations
    // this.service.delete(post.id).subsrcibe();
    this.service.delete(post.id); // promise
  }

}

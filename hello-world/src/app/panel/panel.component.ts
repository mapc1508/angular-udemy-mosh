import { Component, OnInit } from '@angular/core';

@Component({
  // if building reusable components, prefix them, eg: bootstrap-
  selector: 'bootstrap-panel',
  templateUrl: './panel.component.html',
  styleUrls: ['./panel.component.css']
})
export class PanelComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
